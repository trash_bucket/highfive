# ETAPAS #

# 1 - Instalação #

	Git: sudo apt install git
	IDE (Atom):
		1 - wget -O atom-amd64.deb https://atom.io/download/deb
		2 - sudo apt install gdebi-core
		3 - sudo gdebi atom-amd64.deb

# 2 - Espaço de trabalho #

	1 - Com o git instalado, fazer o clone deste repositório:
	2 - Assim que for feito o clone, abrir a pasta dentro do Atom e acessar os arquivos;
	3 - Abrir o index.html básico dentro do projeto, e começar o desafio.
	
# 3 - Desafio #

	1 - Alterar o título da página, colocando qualquer texto;
	2 - Alterar a cor do título;
	3 - Criar uma tabela básica de (4 x 10) de produtos, com as seguintes colunas: (Nome, Categoria, Marca, Valor);
	4 - Após feitas as alterações, fazer um commit (com comentário) e dar push no repositório;